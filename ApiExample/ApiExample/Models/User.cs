﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiExample
{
    public class User
    {
        public string Forename { get; set; }
        public string Surname { get; set; }
        public int Age { get; set; }
    }
}
