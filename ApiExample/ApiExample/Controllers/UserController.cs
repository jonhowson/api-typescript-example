﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiExample.Models;
using Microsoft.AspNetCore.Cors;

namespace ApiExample.Controllers
{
    [Route("api/user")]
    public class UserController : Controller
    {
        private readonly List<User> _users = new List<User>
        {
            new User { Forename = "Jeff", Surname = "Smith", Age = 23 },
            new User { Forename = "Jeff", Surname = "Arlington", Age = 43 },
            new User { Forename = "Brian", Surname = "Sausage", Age = 54 },
            new User { Forename = "Claire", Surname = "Smith", Age = 87 },
            new User { Forename = "Helen", Surname = "Rumpleton", Age = 32 },
            new User { Forename = "Mary", Surname = "Bond", Age = 41 },
        };
        
        [HttpGet]
        [Route("users")]
        public ActionResult Getusers()
        {
            return Ok(_users);
        }

        [HttpPost]
        [Route("by-name")]
        public ActionResult GetPeople([FromBody]UserByNameRequest request)
        {
            return Ok(_users.FirstOrDefault(x => string.Equals(x.Forename, request.SearchTerm, StringComparison.InvariantCultureIgnoreCase) || string.Equals(x.Surname, request.SearchTerm, StringComparison.InvariantCultureIgnoreCase)));
        }
    }
}
