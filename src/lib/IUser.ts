export default interface IUser {
    forename: string
    surname: string
    age: number
}