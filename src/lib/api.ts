import axios, { AxiosRequestConfig, AxiosPromise } from 'axios'

const apiBaseUrl = 'https://localhost:5001/'

export class Api {
    get<TResponse>(url: string, options: undefined | ApiOptions = undefined) : AxiosPromise<TResponse> {
        const config: AxiosRequestConfig = {
            baseURL: apiBaseUrl,
        }
        return axios.get(url, config)
    }
    
    post<TRequest, TResponse>(url: string, request: TRequest, options: undefined | ApiOptions = undefined) : AxiosPromise<TResponse> {
        const config: AxiosRequestConfig = {
            baseURL: apiBaseUrl,
            headers: options ? options.headers : undefined,
        }
        return axios.post(url, request, config)
    }
}

export interface ApiOptions {
    headers: Array<String>
}

export default new Api()