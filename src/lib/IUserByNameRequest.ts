export default interface IUserByNameRequest {
    searchTerm: string
}